<?php

namespace App\Models;

use App\Traits\HasPrimaryKeyUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed name
 * @property mixed|string description
 * @property mixed slug
 */
class Role extends Model
{
    use HasFactory, HasPrimaryKeyUuid;

    protected $table;
    protected $hidden = ['created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        $this->table = config('acl.table_prefix') . 'roles';
        parent::__construct($attributes);
    }
}
