<?php

namespace App\Models;

use App\Traits\HasPrimaryKeyUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed role_id
 * @property int|mixed user_id
 */
class RoleUser extends Model
{
    use HasFactory, HasPrimaryKeyUuid;

    protected $table;
    protected $hidden = ['created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        $this->table = config('acl.table_prefix') . 'role_users';
        parent::__construct($attributes);
    }

}
