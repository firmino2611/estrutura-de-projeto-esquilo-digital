<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $service;
    private $request;

    public function __construct(AuthService $service, Request $request)
    {
        $this->service = $service;
        $this->request = $request;
    }

    /**
     * Faz a autenticação do usuário usando o
     * [AuthService]
     */
    public function login()
    {
        return $this->service->login(
            $this->request->all()
        );
    }

    /**
     * Recupera o usuario com base no token
     * @return JsonResponse
     */
    public function me()
    {
        return $this->service->me();
    }

    public function refresh()
    {
        return $this->service->refresh();
    }
}
