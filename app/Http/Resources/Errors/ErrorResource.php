<?php

namespace App\Http\Resources\Errors;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed errors
 */
class ErrorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param    $request
     * @return   array
     */
    public function toArray($request)
    {
        return [
            'errors' => $this['errors']
        ];
    }
}
