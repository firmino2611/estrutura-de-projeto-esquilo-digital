<?php /** @noinspection PhpUndefinedFieldInspection */

namespace App\Http\Resources;

use App\Traits\Resources\ResourcesTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


class UserResource extends JsonResource
{
    use ResourcesTrait;

    /**
     * Transform the resource into an array.
     *
     * @param   $request
     * @return  array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'email' => $this->email,
                'name'  => $this->name,
                'cpf'   => $this->cpf,
            ]
        ];
    }

    /**
     * Adicionar dados a mais na resposta
     * @param   $request
     * @return  array|string[]
     */
    public function with($request)
    {
        return [
            'example' => 'test'
        ];
    }
}
