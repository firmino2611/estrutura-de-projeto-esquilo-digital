<?php

namespace App\Http\Validators;

use Illuminate\Support\Facades\Validator;

/**
 * Class AuthValidator
 * Para saber todas as regras validas consulte
 * https://laravel.com/docs/8.x/validation#available-validation-rules
 * @package App\Http\Validators
 *
 */
class AuthValidator
{
    public $errors;

    public function validate(array $data)
    {
        $validator = Validator::make($data,
            self::rules(),
            self::messages()
        );
        $this->errors = $validator->fails() ? $validator->errors() : [];
        return !$validator->fails();
    }

    /**
    * Define as regras de validação
    * @return array
    *
    */
    static private function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];
    }

    /**
    * Customiza as respostas de erros
    * @return array
    *
    */
    static private function messages()
    {
        return [

        ];
    }
}
