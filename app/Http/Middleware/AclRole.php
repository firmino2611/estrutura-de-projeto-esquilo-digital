<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AclRole
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param   $role
     * @return  mixed
     */
    public function handle(Request $request, Closure $next, $role)
    {
        if (!$request->user()->hasRole($role)) {
            return response()->json(
                array(
                    'error' => [
                        'authorization' => 'Acesso negado'
                    ]
                ), 401
            );
        }
        return $next($request);
    }
}
