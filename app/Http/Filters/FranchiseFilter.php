<?php

namespace App\Http\Filters;

use App\Repositories\Criteria\CriteriaDefinition;

class FranchiseFilter implements FilterInterface
{
    const FILTERS = [
        'name_social' => CriteriaDefinition::SET_WHERE_LIKE,
        'doc' => CriteriaDefinition::SET_WHERE_LIKE
    ];

    /**
     * @override
     * @return string[]
     */
    static public function getFilters()
    {
        return array(
            'name_social' => 'Nome',
            'doc' => 'Documento'
        );
    }
}
