<?php

namespace App\Http\Filters;

use App\Repositories\Criteria\CriteriaDefinition;

class UserFilter implements FilterInterface
{
    const FILTERS = [
        'name' => CriteriaDefinition::SET_WHERE_LIKE,
        'user' => CriteriaDefinition::SET_WHERE_LIKE,
    ];

    static public function getFilters(): array
    {
        return [
            'name' => 'Nome',
            'user' => 'Usuário'
        ];
    }
}
