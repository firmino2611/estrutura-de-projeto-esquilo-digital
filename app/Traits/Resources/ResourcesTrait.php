<?php

namespace App\Traits\Resources;

trait ResourcesTrait
{
    /**
     * // todo: Adaptar melhor este metodos para retornar os dados com os Resources
     * @param $request
     * @return array
     */
    private function generateRelations($request)
    {
        $relations = [];

        foreach (explode(',', $request->get('with')) as $item) {
            if (!is_null($this->{$request->get('with')}))
                $relations[$item] = $this->when(
                    $request->exists('with'),
                    $this->{$item}
                );
        }
        return $relations;
    }

    public function getRelation($relationship, $request)
    {
        return in_array($relationship, explode(',', $request['with']));
    }

}
