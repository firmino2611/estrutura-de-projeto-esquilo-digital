<?php

namespace App\Services;

use App\Http\Resources\Errors\ErrorResource;
use App\Http\Resources\UserResource;
use App\Http\Validators\AuthValidator;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Http\JsonResponse;

class AuthService
{
    private $validator;
    private $userRepository;

    public function __construct(
        AuthValidator $validator,
        UserRepository $repository
    )
    {
        $this->validator = $validator;
        $this->userRepository = $repository;
    }

    /**
     * Faz a validação de qual metodo de login deve ser usado
     * @param $data
     * @return JsonResponse
     */
    public function login($data)
    {
        return $this->loginWithEmail($data);
    }

    /**
     * Faz a autenticação e recupera o
     * token do usuario autenticado
     *
     * @param   array $data
     * @return  JsonResponse
     */
    public function loginWithEmail(array $data)
    {
        if ($this->validator->validate($data)) {
            if (!$token = auth()->attempt($data)) {
                return response()->json(
                    new ErrorResource(
                        array('errors' => [
                            'authorization' => ['Usuário não autorizado']
                        ])
                    ), 200);
            }

            return response()->json(
                array('data' => $this->respondWithToken($token))
            );
        } else {
            return response()->json(
                new ErrorResource([
                    'errors' => $this->validator->errors
                ]), 200
            );
        }
    }

    /**
     * Recupera o usuário autenticado.
     *
     * @return JsonResponse
     */
    public function me()
    {
        try {
            return response()->json(
                new UserResource(auth()->user())
            );
        } catch (Exception $error) {
            return response()->json(
                new ErrorResource(
                    array('errors' => [
                        'authorization' => 'Token espirado, por favor faça login novamente'
                    ])
                ), 401
            );
        }

    }

    /**
     * Desloga o usuário e invalida o token.
     *
     * @return array
     */
    public function logout()
    {
        auth()->logout();

        return ['message' => 'Successfully logged out'];
    }

    /**
     * Recarrega o token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return response()->json(
            array(
                'data' => $this->respondWithToken(auth()->refresh())
            ), 200
        );

    }

    /**
     * Retorna o token na estrutura padrão
     *
     * @param string $token
     *
     * @return array
     */
    protected function respondWithToken(string $token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
        ];
    }
}
