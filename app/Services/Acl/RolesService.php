<?php

namespace App\Services\Acl;

/**
 * Class RolesService
 * Define as regras de acl existentes no sistema
 * @package App\Services\Acl
 */
class RolesService
{
    // Administrador geral do sistema
    const MASTER = 'master';
    // Franquia
    const FRANCHISE = 'franchise';
    // Funcionario da franquia
    const EMPLOYEE = 'employee';
    // Cliente da franquia
    const CLIENT = 'client';
    // Usuário/cliente do cliente da franqui
    const USER = 'user';

    /**
     * Retorna uma lista das regras de acesso
     * disponiveis no sistema
     * @return string[]
     */
    static public function listRolesSystem()
    {
        return [
            self::MASTER,
            self::FRANCHISE,
            self::EMPLOYEE,
            self::CLIENT,
            self::USER
        ];
    }
}
