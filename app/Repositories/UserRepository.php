<?php

namespace App\Repositories;

use App\Http\Filters\UserFilter;
use App\Models\User;
use App\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class UserRepository extends BaseRepository implements RepositoryInterface
{

    public function __construct(Request $request, UserFilter $userFilters)
    {
        $this->model = User::class;
        $this->query = $this->newQuery();
        $this->request = $request;
        $this->filters = $userFilters;
    }

    /**
     * Cria um novo registro no banco
     * @param array $data
     *
     * @return Model
     */
    public function store(array $data): Model
    {
        return $this->query->create($data);
    }

    /**
     * Recupera todos os registros do banco de dados
     *
     */
    public function listAll()
    {
        $this->query->where(
            'franchise_id',
            auth()->user()->franchise->id
        );

        return $this->doQuery();
    }

    /**
     * Busca e atualiza um registro atraves do ID
     * @param string $id
     * @param array $data
     * @return Model
     */
    public function updateById(string $id, array $data): Model
    {
        $this->query->find($id)->update($data);
        return $this->getById($id);
    }

    /**
     * Busca e exclui um registro atraves do ID
     * @param string $id
     * @return bool|int|mixed|null
     * @throws \Exception
     */
    public function deleteById(string $id)
    {
        $user = $this->getById($id);
        $user->delete();
        return $user;
    }

    /**
     * Busca um registro atraves do ID
     * @param string $id
     * @return Model
     */
    public function getById(string $id)
    {
        return $this->query->find($id);
    }

    /**
     * Recupera o usuário pelo cpf
     * @param string $user
     * @return mixed
     */
    public function getByUser(string $user)
    {
        $this->query->where('user', $user);
        return $this->doQuery()->first();
    }
}
